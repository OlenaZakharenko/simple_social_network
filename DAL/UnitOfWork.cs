﻿using DAL.Contexts;
using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public IUserRepository User { get; private set; }
        public IFriendshipRepository Friendship { get; private set; }
        public IUserProfileRepository UserProfile { get; private set; }
        public ICountryRepository Country { get; private set; }
        public ICityRepository City { get; private set; }
        public IUserCountryRepository UserCountry { get; private set; }
        public IChallengeRepository Challenge { get; private set; }
        public IUserChallengeRepository UserChallenge { get; private set; }
        public IUserFileRepository UserFile { get; private set; }

        private readonly SocialNetworkDbContext _context;

        public UnitOfWork(SocialNetworkDbContext context)
        {
            _context = context;

            User = new UserRepository(_context);
            Friendship = new FriendshipRepository(_context.Friendship, _context);
            UserProfile = new UserProfileRepository(_context.UserProfiles, _context);
            Country = new CountryRepository(_context.Country, _context);
            City = new CityRepository(_context.City, _context);
            Challenge = new ChallengeRepository(_context.Challenge, _context);
            UserChallenge = new UserChallengeRepository(_context.UserChallenge, _context);
            UserCountry = new UserCountryRepository(_context.UserCountry, _context);
            UserFile = new UserFileRepository(_context.UserFile, _context);
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
