﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class Three : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 6,
                column: "ChallengeDescription",
                value: "Go to the famous Hamburg Fish Market early in the morning and, with the spirit of a true Hamburg fishmonger, belt out your best sales pitch in song. Try to sell imaginary fish to strangers using your most dramatic opera voice, and see if anyone “bites”!");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 7,
                column: "ChallengeDescription",
                value: "Visit Miniatur Wunderland and pretend to be a tour guide for the miniature world. Narrate each little scene with dramatic flair, making up wild backstories for the tiny figures and trains.");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 9,
                column: "ChallengeDescription",
                value: "Dress up in a homemade gladiator outfit (even just a cardboard helmet and a paper sword will do!) and challenge tourists to a thumb war battle right outside the Colosseum. Give an epic intro speech, just like the gladiators would, and see if you can get a crowd to cheer you on. ");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 10,
                column: "ChallengeDescription",
                value: "Head to St. Mark’s Square and try to communicate with the pigeons by mimicking their coos and movements. Attempt to \"train\" them to follow you in a line or even perform a mini \"pigeon parade.\" Bonus points if you get tourists to join in, making it a pigeon-inspired flash mob!");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 11,
                column: "ChallengeDescription",
                value: "Buy a Venetian carnival mask, put it on, and roam around popular spots like St. Mark's Square. Try to get as many people as possible to take photos with you, all while staying in mysterious character and refusing to speak.");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 12,
                column: "ChallengeDescription",
                value: "Visit a local shop or street market and buy a variety of unique Dutch snacks (think herring, stroopwafels, licorice, and bitterballen). Challenge strangers to play a “Dutch Snack Dare” game where they close their eyes, choose a random snack, and take a bite. Capture their reactions and see who’s brave enough to take on the herring!");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 13,
                column: "ChallengeDescription",
                value: "In a busy area, try to gather a few people to create a synchronized “bike ballet” where you all ride in a coordinated circle, figure-eight, or other simple formation. Play some classical music on a speaker, and perform this “dance” for amused onlookers. Bonus points if random cyclists join in!\r\n");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 15,
                column: "ChallengeDescription",
                value: "Treat the Erasmus Bridge as your personal runway! Strut across the bridge with your most exaggerated model walk, doing spins and poses as if you're in a fashion show. Try to get strangers to join in or act as your “paparazzi” snapping photos. Bonus points if you can get someone to give you a \"scorecard\" rating for your style!");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 6,
                column: "ChallengeDescription",
                value: "Try to find the best fish sandwich in the city and rate it");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 7,
                column: "ChallengeDescription",
                value: "Take a boat ride on the Alster and feed the ducks");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 9,
                column: "ChallengeDescription",
                value: "Try to recreate the perfect pizza margherita");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 10,
                column: "ChallengeDescription",
                value: "Take a gondola ride and sing a famous Italian song");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 11,
                column: "ChallengeDescription",
                value: "Find the best gelato shop in the city and try every flavor");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 12,
                column: "ChallengeDescription",
                value: "Try to ride a bike without hands for 10 seconds");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 13,
                column: "ChallengeDescription",
                value: "Visit the famous Red Light District and take a selfie with a prostitute");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 15,
                column: "ChallengeDescription",
                value: "Navigate the city while wearing oversized wooden clogs. The clogs could be decorated in a fun and creative way, such as with Dutch flags or other Rotterdam-inspired designs. The challenge could involve completing a scavenger hunt or visiting certain landmarks while wearing the clogs.");
        }
    }
}
