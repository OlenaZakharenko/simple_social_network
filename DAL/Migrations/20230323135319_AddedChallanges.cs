﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddedChallanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChallengeStatus",
                table: "Challenge");

            migrationBuilder.CreateTable(
                name: "UserChallenge",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ChallengeStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ChallengeId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserChallenge", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserChallenge_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserChallenge_Challenge_ChallengeId",
                        column: x => x.ChallengeId,
                        principalTable: "Challenge",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Challenge",
                columns: new[] { "Id", "ChallengeDescription", "CityId" },
                values: new object[,]
                {
                    { 1, "Try to pronounce 'Scheveninger Straße' perfectly", 1 },
                    { 2, "Visit the Berlin Wall and take a selfie with a random stranger", 1 },
                    { 3, "Try to find the best fish sandwich in the city and rate it", 2 },
                    { 4, "Take a boat ride on the Alster and feed the ducks", 2 },
                    { 5, "Learn how to make homemade pasta from a local", 3 },
                    { 6, "Try to recreate the perfect pizza margherita", 3 },
                    { 7, "Take a gondola ride and sing a famous Italian song", 4 },
                    { 8, "Find the best gelato shop in the city and try every flavor", 4 },
                    { 9, "Try to ride a bike without hands for 10 seconds", 5 },
                    { 10, "Visit the famous Red Light District and take a selfie with a prostitute", 5 },
                    { 11, "Navigate Rotterdam using a map or directions that are completely upside down or reversed. This could be done with a mirror, flipping the map upside down.", 6 },
                    { 12, "Navigate the city while wearing oversized wooden clogs. The clogs could be decorated in a fun and creative way, such as with Dutch flags or other Rotterdam-inspired designs. The challenge could involve completing a scavenger hunt or visiting certain landmarks while wearing the clogs.", 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserChallenge_ChallengeId",
                table: "UserChallenge",
                column: "ChallengeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChallenge_UserId",
                table: "UserChallenge",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserChallenge");

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.AddColumn<string>(
                name: "ChallengeStatus",
                table: "Challenge",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
