﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class First2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 1,
                column: "ChallengeDescription",
                value: "Stand near a piece of the Berlin Wall and pretend you’re stuck behind an invisible wall. See if you can get someone to “help” you get out.");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 2,
                column: "ChallengeDescription",
                value: "Go to the Brandenburg Gate and try to freeze in a funny pose for as long as possible while keeping a straight face.");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Walk down Unter den Linden (the famous boulevard) and give as many strangers random compliments as possible in a mix of German and English.", 1 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Go to Museum Island and pretend to be a tour guide, giving strangers completely made-up (but convincing) historical facts about random artifacts.", 1 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "When you spot a street performer, join in with your best dance moves and see if they’ll play along!", 1 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to find the best fish sandwich in the city and rate it", 2 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Take a boat ride on the Alster and feed the ducks", 2 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Learn how to make homemade pasta from a local", 3 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to recreate the perfect pizza margherita", 3 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Take a gondola ride and sing a famous Italian song", 4 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Find the best gelato shop in the city and try every flavor", 4 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to ride a bike without hands for 10 seconds", 5 });

            migrationBuilder.InsertData(
                table: "Challenge",
                columns: new[] { "Id", "ChallengeDescription", "CityId" },
                values: new object[,]
                {
                    { 13, "Visit the famous Red Light District and take a selfie with a prostitute", 5 },
                    { 14, "Navigate Rotterdam using a map or directions that are completely upside down or reversed. This could be done with a mirror, flipping the map upside down.", 6 },
                    { 15, "Navigate the city while wearing oversized wooden clogs. The clogs could be decorated in a fun and creative way, such as with Dutch flags or other Rotterdam-inspired designs. The challenge could involve completing a scavenger hunt or visiting certain landmarks while wearing the clogs.", 6 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 1,
                column: "ChallengeDescription",
                value: "Try to pronounce 'Scheveninger Straße' perfectly");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 2,
                column: "ChallengeDescription",
                value: "Visit the Berlin Wall and take a selfie with a random stranger");

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to find the best fish sandwich in the city and rate it", 2 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Take a boat ride on the Alster and feed the ducks", 2 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Learn how to make homemade pasta from a local", 3 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to recreate the perfect pizza margherita", 3 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Take a gondola ride and sing a famous Italian song", 4 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Find the best gelato shop in the city and try every flavor", 4 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Try to ride a bike without hands for 10 seconds", 5 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Visit the famous Red Light District and take a selfie with a prostitute", 5 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Navigate Rotterdam using a map or directions that are completely upside down or reversed. This could be done with a mirror, flipping the map upside down.", 6 });

            migrationBuilder.UpdateData(
                table: "Challenge",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "ChallengeDescription", "CityId" },
                values: new object[] { "Navigate the city while wearing oversized wooden clogs. The clogs could be decorated in a fun and creative way, such as with Dutch flags or other Rotterdam-inspired designs. The challenge could involve completing a scavenger hunt or visiting certain landmarks while wearing the clogs.", 6 });
        }
    }
}
