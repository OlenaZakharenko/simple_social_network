﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class MigrationWithCustomSql : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Inserting roles into the AspNetRoles table
            migrationBuilder.Sql(@"
                INSERT INTO dbo.AspNetRoles (Name, NormalizedName, ConcurrencyStamp)
                VALUES ('admin', 'ADMIN', null), ('user', 'USER', null);
            ");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM dbo.AspNetRoles
                WHERE Name IN ('admin', 'user');
            ");
        }
    }
}
