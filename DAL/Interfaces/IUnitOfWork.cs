﻿using System;
using System.Threading.Tasks;
using DAL.Interfaces;

namespace DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository User { get; }
        IFriendshipRepository Friendship { get; }
        IUserProfileRepository UserProfile { get; }
        ICountryRepository Country { get; }
        ICityRepository City { get; }
        IChallengeRepository Challenge { get; }
        IUserCountryRepository UserCountry { get; }
        IUserFileRepository UserFile { get; }
        Task SaveChangesAsync();
    }
}
