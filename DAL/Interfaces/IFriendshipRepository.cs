﻿using DAL.Entities;
namespace DAL.Interfaces
{
    public interface IFriendshipRepository : IRepository<Friendship>
    {
    }
}
