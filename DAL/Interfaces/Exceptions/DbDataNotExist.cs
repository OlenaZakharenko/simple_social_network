﻿using System;

namespace DAL.Exceptions
{
    public class DbDataNotExist : ArgumentException
    {
        public DbDataNotExist()
        {

        }

        public DbDataNotExist(string message, string paramName) : base(message, paramName)
        {

        }

    }
}
