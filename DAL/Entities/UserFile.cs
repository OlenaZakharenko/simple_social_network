﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class UserFile : BaseEntity
    {
        public string FileName { get; set; }
        public int UserId { get; set; }
        public int TaskId { get; set; }
        public User User { get; set; }
        public Challenge Challenge { get; set; }
    }
}
