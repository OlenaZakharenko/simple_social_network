﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class User : IdentityUser<int>
    {
        public UserProfile UserProfile { get; set; }
        public ICollection<Friendship> Friendships { get; set; }
        public ICollection<Country> Countries { get; set; }
    }
}
