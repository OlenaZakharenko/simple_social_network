﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class UserCountry : BaseEntity
    {
        public int UserId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryStatus { get; set; }
        public User User { get; set; }
        public Country Country { get; set; }

    }
}
