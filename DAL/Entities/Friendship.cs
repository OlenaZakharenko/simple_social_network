﻿namespace DAL.Entities
{
    public class Friendship : BaseEntity
    {
        public int FriendId { get; set; }
        public int UserId { get; set; }
    }
}
