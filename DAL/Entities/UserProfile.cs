﻿namespace DAL.Entities
{
    public class UserProfile : BaseEntity
    {
        public string City { get; set; }
        public int Age { get; set; }
        public string Job { get; set; }
        public string Interests { get; set; }
        public string Skills { get; set; }
    }
}
