﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Challenge : BaseEntity
    {
        public string ChallengeDescription { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
    }
}
