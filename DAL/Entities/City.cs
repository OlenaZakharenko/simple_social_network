﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class City : BaseEntity
    {
        public string CityName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public ICollection<Challenge> Challenges { get; set; }
    }
}
