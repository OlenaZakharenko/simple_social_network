﻿using Microsoft.EntityFrameworkCore;
using DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace DAL.Contexts
{
    public class SocialNetworkDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public SocialNetworkDbContext(DbContextOptions<SocialNetworkDbContext> options) : base(options)
        { }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Friendship> Friendship { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Challenge> Challenge { get; set; }
        public DbSet<UserChallenge> UserChallenge { get; set; }
        public DbSet<UserCountry> UserCountry { get; set; }
        public DbSet<UserFile> UserFile { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed countries
            modelBuilder.Entity<Country>().HasData(
                new Country { Id = 1, CountryName = "Germany" },
                new Country { Id = 2, CountryName = "Italy" },
                new Country { Id = 3, CountryName = "Netherlands" }
            );

            // Seed cities
            modelBuilder.Entity<City>().HasData(
                new City { Id = 1, CityName = "Berlin", CountryId = 1 },
                new City { Id = 2, CityName = "Hamburg", CountryId = 1 },
                new City { Id = 3, CityName = "Rome", CountryId = 2 },
                new City { Id = 4, CityName = "Venice", CountryId = 2 },
                new City { Id = 5, CityName = "Amsterdam", CountryId = 3 },
                new City { Id = 6, CityName = "Rotterdam", CountryId = 3 }
             );

            // Seed challenges
            modelBuilder.Entity<Challenge>().HasData(
                // Berlin challenges
                new Challenge { Id = 1, ChallengeDescription = "Stand near a piece of the Berlin Wall and pretend you’re stuck behind an invisible wall. See if you can get someone to “help” you get out.", CityId = 1 },
                new Challenge { Id = 2, ChallengeDescription = "Go to the Brandenburg Gate and try to freeze in a funny pose for as long as possible while keeping a straight face.", CityId = 1 },
                new Challenge { Id = 3, ChallengeDescription = "Walk down Unter den Linden (the famous boulevard) and give as many strangers random compliments as possible in a mix of German and English.", CityId = 1 },
                new Challenge { Id = 4, ChallengeDescription = "Go to Museum Island and pretend to be a tour guide, giving strangers completely made-up (but convincing) historical facts about random artifacts.", CityId = 1 },
                new Challenge { Id = 5, ChallengeDescription = "When you spot a street performer, join in with your best dance moves and see if they’ll play along!", CityId = 1 },
                // Hamburg challenges
                new Challenge { Id = 6, ChallengeDescription = "Go to the famous Hamburg Fish Market early in the morning and, with the spirit of a true Hamburg fishmonger, belt out your best sales pitch in song. Try to sell imaginary fish to strangers using your most dramatic opera voice, and see if anyone “bites”!", CityId = 2 },
                new Challenge { Id = 7, ChallengeDescription = "Visit Miniatur Wunderland and pretend to be a tour guide for the miniature world. Narrate each little scene with dramatic flair, making up wild backstories for the tiny figures and trains.", CityId = 2 },
                // Rome challenges
                new Challenge { Id = 8, ChallengeDescription = "Learn how to make homemade pasta from a local", CityId = 3 },
                new Challenge { Id = 9, ChallengeDescription = "Dress up in a homemade gladiator outfit (even just a cardboard helmet and a paper sword will do!) and challenge tourists to a thumb war battle right outside the Colosseum. Give an epic intro speech, just like the gladiators would, and see if you can get a crowd to cheer you on. ", CityId = 3 },
                // Venice challenges
                new Challenge { Id = 10, ChallengeDescription = "Head to St. Mark’s Square and try to communicate with the pigeons by mimicking their coos and movements. Attempt to \"train\" them to follow you in a line or even perform a mini \"pigeon parade.\" Bonus points if you get tourists to join in, making it a pigeon-inspired flash mob!", CityId = 4 },
                new Challenge { Id = 11, ChallengeDescription = "Buy a Venetian carnival mask, put it on, and roam around popular spots like St. Mark's Square. Try to get as many people as possible to take photos with you, all while staying in mysterious character and refusing to speak.", CityId = 4 },
                // Amsterdam challenges
                new Challenge { Id = 12, ChallengeDescription = "Visit a local shop or street market and buy a variety of unique Dutch snacks (think herring, stroopwafels, licorice, and bitterballen). Challenge strangers to play a “Dutch Snack Dare” game where they close their eyes, choose a random snack, and take a bite. Capture their reactions and see who’s brave enough to take on the herring!", CityId = 5 },
                new Challenge { Id = 13, ChallengeDescription = "In a busy area, try to gather a few people to create a synchronized “bike ballet” where you all ride in a coordinated circle, figure-eight, or other simple formation. Play some classical music on a speaker, and perform this “dance” for amused onlookers. Bonus points if random cyclists join in!\r\n", CityId = 5 },
                // Rotterdam challenges
                new Challenge { Id = 14, ChallengeDescription = "Navigate Rotterdam using a map or directions that are completely upside down or reversed. This could be done with a mirror, flipping the map upside down.", CityId = 6 },
                new Challenge { Id = 15, ChallengeDescription = "Treat the Erasmus Bridge as your personal runway! Strut across the bridge with your most exaggerated model walk, doing spins and poses as if you're in a fashion show. Try to get strangers to join in or act as your “paparazzi” snapping photos. Bonus points if you can get someone to give you a \"scorecard\" rating for your style!", CityId = 6 }
           
                );
        }
    }
}