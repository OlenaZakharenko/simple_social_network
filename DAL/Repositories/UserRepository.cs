﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using DAL.Exceptions;
using System;

namespace DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected DbSet<User> DbSet { get; }
        protected SocialNetworkDbContext context;

        public UserRepository(SocialNetworkDbContext context)
        {
            DbSet = context.Set<User>();
            this.context = context;
        }

        protected IQueryable<User> DbSetWithAllProperties()
        {
            return DbSet.Include(x => x.UserProfile);
        }

        public async Task<User> GetUserByUsernameAsync(string userFullname) 
        {

            var entity = await DbSetWithAllProperties()
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.UserName == userFullname);

            if (entity is null)
            {
                throw new DbDataNotExist("There is no user with name", userFullname);
            }

            return entity;
        }

        public async Task<User> GetByIdAsync(int id)
        {
            var entity = await DbSetWithAllProperties()
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
            {
                throw new DbDataNotExist("There is no user with id: ", id.ToString());
            }

            return entity;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var allUsers = await DbSetWithAllProperties()
                .AsNoTracking()
                .ToListAsync();
            return allUsers;
        }

        public async Task<User> AddAsync(User entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in create method in repository");
            }

            var result = await DbSet.AddAsync(entity);
            context.SaveChanges();
            return result.Entity;
        }

        public void Update(User entity)
        {
            context.Users.Update(entity);
            context.SaveChanges();
        }

        public void Delete(User entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in remove method in repository");
            }

                DbSet.Remove(entity);
                context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await GetByIdAsync(id);
            await Task.Run(() => DbSet.Remove(entity));
            context.SaveChanges();
        }

    }
}
