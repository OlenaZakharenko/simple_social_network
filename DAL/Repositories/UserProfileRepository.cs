﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserProfileRepository : GenericRepository<UserProfile>, IUserProfileRepository
    {

        public UserProfileRepository(DbSet<UserProfile> dbSet, SocialNetworkDbContext context) : base(dbSet, context)
        {

        }

        public override void Update(UserProfile entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in update method in repository");
            }

            context.Entry(DbSet.FirstOrDefault(x => x.Id == entity.Id)).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public override async Task<UserProfile> AddAsync(UserProfile entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in create entity method in repository");
            }

            var result = await DbSet.AddAsync(entity);
            context.SaveChanges();
            return result.Entity;
        }
    }
}