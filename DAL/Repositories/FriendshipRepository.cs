﻿using DAL.Contexts;
using DAL.Interfaces;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class FriendshipRepository : GenericRepository<Friendship>, IFriendshipRepository
    {
        public FriendshipRepository(DbSet<Friendship> dbSet, SocialNetworkDbContext context) : base(dbSet, context)
        {

        }


    }
}
