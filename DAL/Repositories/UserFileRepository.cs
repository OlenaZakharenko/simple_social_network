﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserFileRepository : GenericRepository<UserFile>, IUserFileRepository
    {
        public UserFileRepository(DbSet<UserFile> dbSet, SocialNetworkDbContext context) : base(dbSet, context)
        {

        }
    }
}
