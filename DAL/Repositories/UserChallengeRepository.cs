﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserChallengeRepository : GenericRepository<UserChallenge>, IUserChallengeRepository
    {
        public UserChallengeRepository(DbSet<UserChallenge> dbSet, SocialNetworkDbContext context) : base(dbSet, context)
        {

        }
    }
}
