﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Exceptions;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public  class GenericRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected DbSet<T> DbSet { get; }
        protected SocialNetworkDbContext context;

        protected GenericRepository(DbSet<T> dbSet, SocialNetworkDbContext context)
        {
            DbSet = dbSet;
            this.context = context;
        }

        protected IQueryable<T> DbSetWithAllProperties()
        {
            return DbSet;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var entity = await DbSetWithAllProperties()
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
            {
                throw new DbDataNotExist("There is no record found with id: ", id.ToString());
            }

            return entity;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await DbSetWithAllProperties()
                .AsNoTracking()
                .ToListAsync();

            return entities;
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in create entity method in repository");
            }

            var result = await DbSet.AddAsync(entity);
            context.SaveChanges();
            return result.Entity;
        }

        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in update method in repository");
            }

            context.Entry(DbSet.FirstOrDefault(x => x.Id == entity.Id)).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Error while checking for null in delete method in repository");
            }
            DbSet.Remove(entity);
            context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await GetByIdAsync(id);
            await Task.Run(() => DbSet.Remove(entity));
        }
    }
}
