﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class FriendshipService : BaseService, IFriendshipService
    {
        private readonly IUserService _userService;
        public FriendshipService(IUnitOfWork unitOfWork, IMapper mapper, IUserService userService) : base(unitOfWork, mapper)
        {
            _userService = userService;
        }

        public async Task<IEnumerable<UserDTO>> SearchFriendsAsync(FilterSearchDTO filterSearchDTO)
        {
            var result = new List<UserDTO>();
            var users = await _userService.GetUsersAsync();

            if (filterSearchDTO.UserName != null) 
            {
                var res = users.Where(x => x.UserName == filterSearchDTO.UserName).ToList();
                foreach (var item in res)
                {
                    result.Add(item);
                }
            }

            if (filterSearchDTO.City != null)
            {
                if(result.Count != 0)
                {
                    var result1 = result;
                    result1 = result.Where(x => x.UserProfileDTO.City == filterSearchDTO.City).ToList();
                    result.Clear();

                    foreach (var item in result1)
                    {
                        result.Add(item);
                    }
                }
                else
                {
                    var res = users.Where(x => x.UserProfileDTO.City == filterSearchDTO.City).ToList();
                    foreach (var item in res)
                    {
                        result.Add(item);
                    }
                }
            }

            if (filterSearchDTO.Job != null)
            {
                if (result.Count != 0)
                {
                    var result1 = result;
                    result1 = result.Where(x => x.UserProfileDTO.Job == filterSearchDTO.Job).ToList();
                    result.Clear();

                    foreach (var item in result1)
                    {
                        result.Add(item);
                    }
                }

                else
                {
                    var res = users.Where(x => x.UserProfileDTO.Job == filterSearchDTO.Job).ToList();
                    foreach (var item in res)
                    {
                        result.Add(item);
                    }
                }
            }

            if (filterSearchDTO.MinAge != 0 && filterSearchDTO.MaxAge != 0)
            {
                if (result.Count != 0)
                {
                    var result2 = result;
                    result2 = result2.Where(x => x.UserProfileDTO.Age > filterSearchDTO.MinAge && x.UserProfileDTO.Age < filterSearchDTO.MaxAge).ToList();
                    result.Clear();

                    foreach (var item in result2)
                    {
                        result.Add(item);
                    }
                }
                else
                {
                    var res = users.Where(x => x.UserProfileDTO.Age > filterSearchDTO.MinAge && x.UserProfileDTO.Age < filterSearchDTO.MaxAge).ToList();
                    result.Clear();

                    foreach (var item in res)
                    {
                        result.Add(item);
                    }
                }
                   
            }
            return result;
        }
        public async Task<IEnumerable<UserDTO>> GetFriendsByUserIdAsync(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            var friends = new List<UserDTO>();
            var friendships = await GetFriendshipsAsync();
            var selectedFriendships = friendships.Where(t => t.UserId == userId || t.FriendId == userId);
            foreach(var item in selectedFriendships)
            {
                if(item.UserId == userId)
                {
                    var newFriend = await _userService.GetByIdAsync(item.FriendId);
                    friends.Add(newFriend);
                }

                if (item.FriendId == userId)
                {
                    friends.Add(await _userService.GetByIdAsync(item.UserId));
                }
            }
            return friends;
        }

        public async Task AddFriendshipAsync(int friendId, int userId)
        {
            var friendship = new Friendship()
            {
                FriendId = friendId,
                UserId = userId
            };
            
            await UnitOfWork.Friendship.AddAsync(friendship);
        }

        public async Task<UserDTO> GetUserByIdAsync(int id)
        {
            var result = await UnitOfWork.User.GetByIdAsync(id);
            var resultDTO = Mapper.Map<User, UserDTO>(result);
            return resultDTO;
        }
        public async Task<IEnumerable<Friendship>> GetFriendshipsAsync()
        {
            var result = await UnitOfWork.Friendship.GetAllAsync();
            return result;
        }

        public async Task DeleteFriendshipByUserId(int userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            var friends = new List<UserDTO>();
            var friendships = await GetFriendshipsAsync();
            var selectedFriendships = friendships.Where(t => t.UserId == userId || t.FriendId == userId);

            foreach (var friendship in friendships)
            {
                foreach(var friendshipToDelete in selectedFriendships)
                {
                    if(friendship == friendshipToDelete)
                    {
                        UnitOfWork.Friendship.Delete(friendshipToDelete);
                    }
                }
            }
            await UnitOfWork.SaveChangesAsync();
        }
    }
}
