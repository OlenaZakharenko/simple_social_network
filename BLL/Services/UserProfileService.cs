﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserProfileService : BaseService, IUserProfileService
    {
        public UserProfileService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public async Task<UserProfile> AddUserProfile(UserDTO user)
        {
            var userProfileMapped = Mapper.Map<UserProfileDTO, UserProfile>(user.UserProfileDTO);
            var userProfile = await UnitOfWork.UserProfile.AddAsync(userProfileMapped);
            return userProfile;
        }

        public Task UpdateUserProfileAsync(UserDTO userDTO)
        {
            var userProfileMapped = Mapper.Map<UserProfileDTO, UserProfile>(userDTO.UserProfileDTO);
            UnitOfWork.UserProfile.Update(userProfileMapped);
            return UnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteUserProfile(UserProfile userProfile)
        {
            await UnitOfWork.UserProfile.DeleteByIdAsync(userProfile.Id);
            await UnitOfWork.SaveChangesAsync();
        }

        public async Task<UserProfile> GetByIdAsync(int id)
        {
            var userProfile = await UnitOfWork.UserProfile.GetByIdAsync(id);

            return userProfile;
        }
    }
}
