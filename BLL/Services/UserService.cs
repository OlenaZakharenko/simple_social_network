﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }
        public async Task<UserDTO> GetUserByUsernameAsync(string username)
        {
            var result = await UnitOfWork.User.GetUserByUsernameAsync(username);
            var resultDTO = Mapper.Map<User, UserDTO>(result);
            return resultDTO;
        }
        public async Task<IEnumerable<UserDTO>> GetUsersAsync()
        {
            var users = await UnitOfWork.User.GetAllAsync();
            var userProfiles = await UnitOfWork.UserProfile.GetAllAsync();

            foreach( var user in users)
            {
                foreach(var userProfile in userProfiles)
                {
                    if(user.Id == userProfile.Id)
                    {
                        user.UserProfile = userProfile;
                    }
                }
            }

            var userDTOs = Mapper.Map<IEnumerable<User>, IEnumerable<UserDTO>>(users);
            return userDTOs;
        }

        public async Task<UserDTO> GetByIdAsync(int id)
        {
            var user = await UnitOfWork.User.GetByIdAsync(id);
            var userDTO = Mapper.Map<User, UserDTO>(user);

            return userDTO;
        }

        public async Task DeleteUser(UserDTO userDTO)
        {
            var user = Mapper.Map<UserDTO, User>(userDTO);
            UnitOfWork.User.Delete(user);
            await UnitOfWork.SaveChangesAsync();
        }

    }
}
