﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CityService : BaseService, ICityService
    {
        public CityService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public async Task<IEnumerable<CityDTO>> GetCitiesByCountryAsync(int countryId)
        {
            var allCities = await UnitOfWork.City.GetAllAsync();
            var citiesByCountry = allCities.Where(t => t.CountryId == countryId);

            var cityDTOs = Mapper.Map<IEnumerable<City>, IEnumerable<CityDTO>>(citiesByCountry);
            return cityDTOs;
        }

        public async Task<CityDTO> GetByIdAsync(int id)
        {
            var city = await UnitOfWork.City.GetByIdAsync(id);
            var cityDTO = Mapper.Map<City, CityDTO>(city);

            return cityDTO;
        }
    }
}
