﻿using AutoMapper;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class FileUploadService : BaseService, IFileUploadService
    {
        private readonly IWebHostEnvironment _environment;

        public FileUploadService(IWebHostEnvironment environment, IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _environment = environment;
        }

        public async Task<string> SubmitFileForReview(IFormFile file, int userId, int taskId)
        {
            var uploadsFolder = Path.Combine(_environment.WebRootPath, "uploads");

            if (!Directory.Exists(uploadsFolder))
            {
                Directory.CreateDirectory(uploadsFolder);
            }

            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            var filePath = Path.Combine(uploadsFolder, fileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var userFile = new UserFile()
            {
                FileName = fileName,
                UserId = userId,
                TaskId = taskId,
            };
            await UnitOfWork.UserFile.AddAsync(userFile);

            return fileName;
        }
    }
}
