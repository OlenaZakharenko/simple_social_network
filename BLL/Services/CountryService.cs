﻿using AutoMapper;
using BLL.DataTransferObjects;
using DAL.Entities;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;

namespace BLL.Services
{
    public class CountryService : BaseService, ICountryService
    {
        private readonly IUserService _userService;
        public CountryService(IUnitOfWork unitOfWork, IMapper mapper, IUserService userService) : base(unitOfWork, mapper)
        {
            _userService = userService;
        }

        public async Task<IEnumerable<CountryDTO>> GetCountriesAsync()
        {
            var countries = await UnitOfWork.Country.GetAllAsync();

            var countryDTOs = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryDTO>>(countries);
            return countryDTOs;
        }

        public async Task<CountryDTO> GetByIdAsync(int id)
        {
            var country = await UnitOfWork.Country.GetByIdAsync(id);
            var countryDTO = Mapper.Map<Country, CountryDTO>(country);

            return countryDTO;
        }

        public async Task<IEnumerable<CountryDTO>> GetCountriesByStatusAndUserIdAsync(int userId, string countryStatus)
        {
            var user = await _userService.GetByIdAsync(userId);
            var allCountries = await GetCountriesAsync();
            var allUserCountries = await UnitOfWork.UserCountry.GetAllAsync();
            var selectedUserCountries = allUserCountries.Where(t => t.UserId == userId);
            var countries = new List<CountryDTO>();
                
            foreach (var country in selectedUserCountries)
                {
                        if (country.CountryStatus == countryStatus)
                        {
                           var matchedCountry = allCountries.Where(t => t.CountryName == country.CountryName).First();
                           countries.Add(matchedCountry);
                        }
                }

            if(countries.Count != 0)
            {
                return countries;
            }

            else
            {
                return null;
            }
                
            }
        }
    }
