﻿using AutoMapper;
using DAL;

namespace BLL.Services
{
    public class BaseService
    {
        protected IUnitOfWork UnitOfWork { get; set; }
        protected IMapper Mapper { get; set; }

        protected BaseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }
    }
}
