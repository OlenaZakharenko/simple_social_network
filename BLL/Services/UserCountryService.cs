﻿using AutoMapper;
using BLL.DataTransferObjects;
using DAL.Entities;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Buffers.Text;
using System.Reflection.Metadata;
using System.Security.Cryptography;


namespace BLL.Services
{
    public class UserCountryService : BaseService, IUserCountryService
    {
        private readonly ICountryService _countryService;

        public UserCountryService(IUnitOfWork unitOfWork, IMapper mapper, ICountryService countryService) : base(unitOfWork, mapper)
        {
            _countryService = countryService;
        }

        public async Task<IEnumerable<UserCountryDTO>> GetUserCountriesAsync()
        {
            var userCountries = await UnitOfWork.UserCountry.GetAllAsync();

            var userCountryDTOs = Mapper.Map<IEnumerable<UserCountry>, IEnumerable<UserCountryDTO>>(userCountries);
            return userCountryDTOs;
        }

        public async Task<bool> UserHasCurrentCountryAsync(int userId)
        {
            return await _countryService.GetCountriesByStatusAndUserIdAsync(userId, "Current") != null;
        }

        public async Task AddCurrentCountryAsync(int countryId, int userId)
        {
            var country = _countryService.GetByIdAsync(countryId).Result;
                var currentCountry = new UserCountry()
                {
                    CountryId = countryId,
                    UserId = userId,
                    CountryName = country.CountryName,
                    CountryStatus = "Current"
                };

                await UnitOfWork.UserCountry.AddAsync(currentCountry);
        }

        public async Task<bool> IsCountryCurrentAsync(int userId, int countryId)
        {
            var allUserCountries = await UnitOfWork.UserCountry.GetAllAsync();

            if (allUserCountries == null || !allUserCountries.Any())
            {
                return false;
            }

            var isCountryCurrent = allUserCountries.Any(userCountry => userCountry.CountryId == countryId && userCountry.UserId == userId && userCountry.CountryStatus == "Current");
            return isCountryCurrent;
        }
    }
}
