﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ChallengeService : BaseService, IChallengeService
    {
        public ChallengeService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public async Task<IEnumerable<ChallengeDTO>> GetChallengesByCityIdAsync(int cityId)
        {
            var allChallenges = await UnitOfWork.Challenge.GetAllAsync();
            var challengesByCity = allChallenges.Where(t => t.CityId == cityId);

            var challengesDTOs = Mapper.Map<IEnumerable<Challenge>, IEnumerable<ChallengeDTO>>(challengesByCity);
            return challengesDTOs;
        }

        public async Task<ChallengeDTO> GetChallengeByIdAsync(int id)
        {
            var challenge = await UnitOfWork.Challenge.GetByIdAsync(id);
            var challengeDTO = Mapper.Map<Challenge, ChallengeDTO>(challenge);

            return challengeDTO;
        }
    }
}
