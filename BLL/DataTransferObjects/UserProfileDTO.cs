﻿namespace BLL.DataTransferObjects
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        public string City { get; set; }
        public int Age { get; set; }
        public string Job { get; set; }
        public string Interests { get; set; }
        public string Skills { get; set; }
    }
}
