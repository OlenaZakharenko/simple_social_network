﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DataTransferObjects
{
    public class UserChallengeDTO
    {
        public string ChallengeStatus { get; set; }
        public int ChallengeId { get; set; }
        public int UserId { get; set; }
    }
}
