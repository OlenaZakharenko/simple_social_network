﻿namespace BLL.DataTransferObjects
{
    public class FilterSearchDTO
    {
        public string UserName { get; set; }
        public string City { get; set; }
        public int MinAge { get; set; }
        public  int MaxAge { get; set; }
        public string Job { get; set; }
    }
}
