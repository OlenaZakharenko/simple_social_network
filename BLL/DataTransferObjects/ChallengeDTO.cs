﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DataTransferObjects
{
    public class ChallengeDTO
    {
        public string ChallengeDescription { get; set; }
        public int CityId { get; set; }
    }
}
