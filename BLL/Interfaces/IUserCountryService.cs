﻿using BLL.DataTransferObjects;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserCountryService
    {
        public Task<IEnumerable<UserCountryDTO>> GetUserCountriesAsync();
        public Task AddCurrentCountryAsync(int countryId, int userId);
        public Task<bool> UserHasCurrentCountryAsync(int userId);
        public Task<bool> IsCountryCurrentAsync(int userId, int countryId);
    }
}
