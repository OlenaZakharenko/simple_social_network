﻿using BLL.DataTransferObjects;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IFriendshipService
    {
        public Task<IEnumerable<Friendship>> GetFriendshipsAsync();
        public Task<IEnumerable<UserDTO>> GetFriendsByUserIdAsync(int userId);
        public Task<IEnumerable<UserDTO>> SearchFriendsAsync(FilterSearchDTO searchDTO);
        public Task AddFriendshipAsync(int friendId, int userId);
        Task DeleteFriendshipByUserId(int userId);
    }
}
