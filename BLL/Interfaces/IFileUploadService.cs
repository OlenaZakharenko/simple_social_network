﻿using BLL.DataTransferObjects;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IFileUploadService
    {
        public Task<string> SubmitFileForReview(IFormFile file, int userId, int taskId);
    }
}
