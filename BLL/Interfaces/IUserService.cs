﻿using BLL.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        public Task<UserDTO> GetByIdAsync(int id);
        public Task<UserDTO> GetUserByUsernameAsync(string username);
        public Task<IEnumerable<UserDTO>> GetUsersAsync();
       Task DeleteUser(UserDTO userDTO);
    }
}
