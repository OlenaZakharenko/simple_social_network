﻿using BLL.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IChallengeService
    {
        public Task<IEnumerable<ChallengeDTO>> GetChallengesByCityIdAsync(int cityId);
        public Task<ChallengeDTO> GetChallengeByIdAsync(int id);
    }
}
