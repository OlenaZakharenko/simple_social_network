﻿using BLL.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICityService
    {
        public Task<IEnumerable<CityDTO>> GetCitiesByCountryAsync(int countryId);
        public Task<CityDTO> GetByIdAsync(int id);
    }
}
