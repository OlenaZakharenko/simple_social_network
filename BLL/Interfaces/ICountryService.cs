﻿using BLL.DataTransferObjects;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICountryService
    {
        public Task<IEnumerable<CountryDTO>> GetCountriesAsync();
        public Task<CountryDTO> GetByIdAsync(int id);
        public Task<IEnumerable<CountryDTO>> GetCountriesByStatusAndUserIdAsync(int userId, string countryStatus);
        //public Task AddCountryAsync(CountryDTO countryDTO);
    }
}
