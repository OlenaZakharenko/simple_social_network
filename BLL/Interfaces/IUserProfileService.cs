﻿using BLL.DataTransferObjects;
using DAL.Entities;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserProfileService
    {
        public Task<UserProfile> AddUserProfile(UserDTO userDTO);
        public Task UpdateUserProfileAsync(UserDTO userDTO);
        public Task DeleteUserProfile(UserProfile userProfile);
        public Task<UserProfile> GetByIdAsync(int id);
    }
}
