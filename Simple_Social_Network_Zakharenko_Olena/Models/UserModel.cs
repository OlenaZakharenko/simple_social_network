﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; } 
        public string Job { get; set; }
        public string Interests { get; set; }
        public string Skills { get; set; }
        public string City { get; set; }
        public int Age { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
