﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class MessageModel
    {
        public int Id { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public DateTime TimeSent { get; set; } 
        public string MessageContent { get; set; }
    }
}
