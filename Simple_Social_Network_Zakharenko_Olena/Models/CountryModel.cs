﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string CountryStatus { get; set; }
        public ICollection<City> Cities { get; set; }
    }
}
