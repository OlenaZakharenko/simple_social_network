﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class CityModel
    {
        public int Id { get; set; }
        public string CityName { get; set; }
    }
}
