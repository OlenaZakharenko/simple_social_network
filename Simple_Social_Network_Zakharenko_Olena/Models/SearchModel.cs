﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class SearchModel
    {
        public  string UserName { get; set; }
        public string City { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public string Job { get; set; }
    }
}
