﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Models
{
    public class UserChallengeModel
    {
        public string ChallengeStatus { get; set; }
        public int ChallengeId { get; set; }
        public int UserId { get; set; }
    }
}
