﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Simple_Social_Network_Zakharenko_Olena.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Controllers
{
    [Route("api/countries")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryService _countryService;
        private readonly IUserCountryService _userCountryService;
        private readonly IMapper _mapper;
        public CountryController(ICountryService countryService, IUserCountryService userCountryService, IMapper mapper)
        {
            _countryService = countryService;
            _userCountryService = userCountryService;
            _mapper = mapper;
        }

        // GET: api/countries
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<CountryDTO>>> GetCountriesAsync()
        {
            var res = await _countryService.GetCountriesAsync();
            if (res != null)
            {
                return Ok(res);
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<CountryDTO>> GetCountryByIdAsync(int id)
        {
            var res = await _countryService.GetByIdAsync(id);
            var country = _mapper.Map<CountryDTO, CountryModel>(res);

            if (country != null)
            {
                return Ok(country);
            }
            return BadRequest();

        }

        //[HttpGet("{id}")]
        //[Authorize]
        //public async Task<ActionResult<IEnumerable<CountryDTO>>> GetCountriesByStatusAndUserIdAsync(int userId, string countryStatus)
        //{
        //    var countries = await _countryService.GetCountriesByStatusAndUserIdAsync(userId, countryStatus);
        //    return Ok(countries);
        //}

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddCurrentCountryAsync(int[] array)
        {
            try
            {
                var userId = array[0];
                var countryId = array[1];

                var hasCurrentCountry = await _userCountryService.UserHasCurrentCountryAsync(userId);

                if (hasCurrentCountry)
                {
                    return Conflict("User already has a country in the 'Current' section");
                }

                await _userCountryService.AddCurrentCountryAsync(countryId, userId); 

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("isCurrent")]
        [Authorize]
        public async Task<ActionResult<bool>> IsCountryCurrentAsync(int userId, int countryId)
        {
            var isCurrent = await _userCountryService.IsCountryCurrentAsync(userId, countryId);
            return Ok(isCurrent);
        }
    }

}
