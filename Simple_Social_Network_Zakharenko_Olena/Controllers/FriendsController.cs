﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Simple_Social_Network_Zakharenko_Olena.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Controllers
{
    [Route("api/friends")]
    [ApiController]
    public class FriendsController : ControllerBase
    {
        private readonly IFriendshipService _friendshipService;
        private readonly IMapper _mapper;
        public FriendsController(IFriendshipService friendshipService, IMapper mapper)
        {
            _friendshipService = friendshipService;
            _mapper = mapper;
        }

        // Post: api/friends/searchFriend
        [Route("searchFriend")]
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<IEnumerable<UserModel>>> SearchFriendsAsync([FromBody]SearchModel searchModel)
        {
            var searchDTO = _mapper.Map<SearchModel, FilterSearchDTO>(searchModel);
            var res = await _friendshipService.SearchFriendsAsync(searchDTO);
            var result = _mapper.Map<IEnumerable<UserDTO>, IEnumerable<UserModel>>(res);
            if (res != null)
            {
                return Ok(result);
            }
            return BadRequest();
        }


        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetFriendsByUserIdAsync(int id)
        {
            var friends = await _friendshipService.GetFriendsByUserIdAsync(id);
            return Ok(friends);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddFriendAsync(int[] array)
        {
            try
            {
                var userId = array[0];
                var friendId = array[1];
                await _friendshipService.AddFriendshipAsync(friendId, userId); 

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
                throw e;
            }
        }
    }

}
