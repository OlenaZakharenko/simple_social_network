﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Simple_Social_Network_Zakharenko_Olena.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Controllers
{
    [Route("api/cities")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityService _cityService;
        private readonly IMapper _mapper;
        public CityController(ICityService cityService, IMapper mapper)
        {
            _cityService = cityService;
            _mapper = mapper;
        }

        // GET: api/cities
        [HttpGet("{countryId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<CityDTO>>> GetCitiesByCountryAsync(int countryId)
        {
            var res = await _cityService.GetCitiesByCountryAsync(countryId);
            if (res != null)
            {
                return Ok(res);
            }
            return BadRequest();
        }

        //[HttpGet("{id}")]
        //[Authorize]
        //public async Task<ActionResult<CityDTO>> GetCityByIdAsync(int id)
        //{
        //    var res = await _cityService.GetByIdAsync(id);
        //    var city = _mapper.Map<CityDTO, CityModel>(res);

        //    if (city != null)
        //    {
        //        return Ok(city);
        //    }
        //    return BadRequest();

        //}
    }

}
