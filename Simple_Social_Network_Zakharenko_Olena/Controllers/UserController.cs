﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Simple_Social_Network_Zakharenko_Olena.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Controllers
{

    [Produces("application/json")]
    [Route("api/users")]
    [ApiController]

    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly ApplicationSettings _appSettings;
        private readonly IUserProfileService _userProfileService;
        private readonly IFriendshipService _friendshipService;

        public UserController(IFriendshipService friendshipService, IUserProfileService userProfileService, IUserService userService, IMapper mapper, IOptions<ApplicationSettings> appSettings,UserManager<User> userManager)
        {
            _userService = userService;
            _userProfileService = userProfileService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _friendshipService = friendshipService;
        }

        // GET: api/users
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsersAsync()
        {
            var res = await _userService.GetUsersAsync();
            if (res != null)
            {
                return Ok(res);
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UserDTO>> GetUserByIdAsync(int id)
        {
            var res = await _userService.GetByIdAsync(id);
            var user = _mapper.Map<UserDTO, UserModel>(res);

            if (user != null)
            {
                return Ok(user);
            }
            return BadRequest();

        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        //[AllowAnonymous]
        //[Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteUserById(int id)
        {
            await _friendshipService.DeleteFriendshipByUserId(id);
            var user = await _userService.GetByIdAsync(id);
            var userProfile = await _userProfileService.GetByIdAsync(user.UserProfileDTO.Id);
            await _userProfileService.DeleteUserProfile(userProfile);

            return Ok();
        }

        //PATCH: /api/users/
        [HttpPatch]
        [Authorize]
        public async Task<ActionResult> Update(UserModel userModel)
        {
           
            var userDTO = _mapper.Map<UserModel, UserDTO>(userModel);
            userDTO.UserProfileDTO.Id = userDTO.Id;
            var user = _mapper.Map<UserDTO, User>(userDTO);

            try
            {  
                await _userManager.UpdateNormalizedEmailAsync(user);
                await _userManager.UpdateNormalizedUserNameAsync(user);
                var userInUserManager = _userManager.Users.FirstOrDefault(u => u.UserName == userModel.UserName);

                userInUserManager.UserName = user.UserName;
                userInUserManager.NormalizedUserName = user.NormalizedUserName;
                userInUserManager.Email = user.Email;
                userInUserManager.NormalizedEmail = user.NormalizedEmail;
                userInUserManager.SecurityStamp = user.SecurityStamp;
                userInUserManager.UserProfile = user.UserProfile;

                await _userManager.UpdateSecurityStampAsync(userInUserManager);

                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        //api/users/register
        public async Task<ActionResult<UserModel>> PostUser([FromBody] UserModel model)
        {
            var UserEntity = new User()
            {
                UserName = model.UserName,
                Email = model.Email,
                UserProfile = new UserProfile()
                {
                    City = model.City,
                    Age = model.Age,
                    Job = model.Job,
                    Interests = model.Interests,
                    Skills = model.Skills
                }
            };

            var result = await _userManager.CreateAsync(UserEntity, model.Password);
            await _userManager.AddToRoleAsync(UserEntity, model.Role);

            await Login(model);

            return Ok(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        //api/users/login
        public async Task<IActionResult> Login(UserModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)),SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }


    }
}
