﻿using AutoMapper;
using BLL.DataTransferObjects;
using BLL.Interfaces;
using BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Simple_Social_Network_Zakharenko_Olena.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Network_Zakharenko_Olena.Controllers
{
    [Route("api/challenges")]
    [ApiController]
    public class ChallengeController : ControllerBase
    {
        private readonly IChallengeService _challengeService;
        private readonly IFileUploadService _fileUploadService;
        private readonly IMapper _mapper;
        private readonly IUserCountryService _userCountryService;
        public ChallengeController(IMapper mapper, IChallengeService challengeService,IUserCountryService userCountryService, IFileUploadService fileUploadService)
        {
            _mapper = mapper;
            _challengeService = challengeService;
            _fileUploadService = fileUploadService;
            _userCountryService = userCountryService;
        }

        [HttpGet("{cityId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ChallengeDTO>>> GetChallengesByCityIdAsync(int cityId)
        {
            var friends = await _challengeService.GetChallengesByCityIdAsync(cityId);
            return Ok(friends);
        }

        [HttpPost]
        [Route("submitFile")]
        public async Task<IActionResult> SubmitFileForReview(IFormFile file, int userId, int taskId)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("File is null or empty.");
            }

            var fileName = await _fileUploadService.SubmitFileForReview(file, userId, taskId);

            return Ok(new { fileName });
        }

        [HttpGet("isCurrent")]
        [Authorize]
        public async Task<ActionResult<bool>> IsCountryCurrentAsync(int userId, int countryId)
        {
            var isCurrent = await _userCountryService.IsCountryCurrentAsync(userId, countryId);
            return Ok(isCurrent);
        }

        //[HttpGet("{id}")]
        //[Authorize]
        //public async Task<ActionResult<ChallengeDTO>> GetChallengeByIdAsync(int id)
        //{
        //    var res = await _challengeService.GetChallengeByIdAsync(id);
        //    var challenge = _mapper.Map<ChallengeDTO, ChallengeModel>(res);

        //    if (challenge != null)
        //    {
        //        return Ok(challenge);
        //    }
        //    return BadRequest();

        //}

    }
}