﻿using AutoMapper;
using BLL.DataTransferObjects;
using DAL.Entities;
using Simple_Social_Network_Zakharenko_Olena.Models;

namespace Simple_Social_Network_Zakharenko_Olena
{

    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(userDto => userDto.Id, configExpression => configExpression.MapFrom(user => user.Id))
                .ForMember(userDto => userDto.UserName, configExpression => configExpression.MapFrom(user => user.UserName))
                .ForMember(userDto => userDto.Email, configExpression => configExpression.MapFrom(user => user.Email))
                .ForMember(userDto => userDto.UserProfileDTO, configExpression => configExpression.MapFrom(user => user.UserProfile))
                .ReverseMap();

             CreateMap<SearchModel, FilterSearchDTO>()
                 .ForMember(filterSearchDTO => filterSearchDTO.UserName, c => c.MapFrom(searchModel => searchModel.UserName))
                 .ForMember(filterSearchDTO => filterSearchDTO.City, c => c.MapFrom(searchModel => searchModel.City))
                 .ForMember(filterSearchDTO => filterSearchDTO.MinAge, c => c.MapFrom(searchModel => searchModel.MinAge))
                 .ForMember(filterSearchDTO => filterSearchDTO.MaxAge, c => c.MapFrom(searchModel => searchModel.MaxAge))
                 .ReverseMap();

             CreateMap<UserDTO, UserModel>()
                 .ForMember(userModel => userModel.UserName, c => c.MapFrom(userDTO => userDTO.UserName))
                 .ForMember(userModel => userModel.Email, c => c.MapFrom(userDTO => userDTO.Email))
                 .ForMember(userModel => userModel.Age, c => c.MapFrom(userDTO => userDTO.UserProfileDTO.Age))
                 .ForMember(userModel => userModel.City, c => c.MapFrom(userDTO => userDTO.UserProfileDTO.City))
                 .ForMember(userModel => userModel.Interests, c => c.MapFrom(userDTO => userDTO.UserProfileDTO.Interests))
                 .ForMember(userModel => userModel.Job, c => c.MapFrom(userDTO => userDTO.UserProfileDTO.Job))
                 .ForMember(userModel => userModel.Skills, c => c.MapFrom(userDTO => userDTO.UserProfileDTO.Skills))
                 .ForMember(userModel => userModel.Id, c => c.MapFrom(userDTO => userDTO.Id))
                 .ReverseMap();

             CreateMap<CityDTO, CityModel>()
                .ForMember(cityModel => cityModel.CityName, c => c.MapFrom(cityDTO => cityDTO.CityName))
                .ForMember(cityModel => cityModel.Id, c => c.MapFrom(cityDTO => cityDTO.Id))
                .ReverseMap();

            CreateMap<ChallengeDTO, ChallengeModel>()
               .ForMember(challengeModel => challengeModel.ChallengeDescription, c => c.MapFrom(challengeDTO => challengeDTO.ChallengeDescription))
               .ReverseMap();

            CreateMap<UserProfile, UserProfileDTO>()
                .ForMember(userProfileDTO => userProfileDTO.Age, c => c.MapFrom(userProfile => userProfile.Age))
                .ForMember(userProfileDTO => userProfileDTO.City, c => c.MapFrom(userProfile => userProfile.City))
                .ForMember(userProfileDTO => userProfileDTO.Interests, c => c.MapFrom(userProfile => userProfile.Interests))
                .ForMember(userProfileDTO => userProfileDTO.Job, c => c.MapFrom(userProfile => userProfile.Job))
                .ForMember(userProfileDTO => userProfileDTO.Skills, c => c.MapFrom(userProfile => userProfile.Skills))
                .ForMember(userProfileDTO => userProfileDTO.Id, c => c.MapFrom(userProfile => userProfile.Id))
                .ReverseMap();

            CreateMap<Country, CountryDTO>()
                .ForMember(countryDto => countryDto.Id, configExpression => configExpression.MapFrom(country => country.Id))
                .ForMember(countryDto => countryDto.CountryName, configExpression => configExpression.MapFrom(country => country.CountryName))
                .ReverseMap();

            CreateMap<Challenge, ChallengeDTO>()
                .ForMember(challengeDto => challengeDto.ChallengeDescription, configExpression => configExpression.MapFrom(challenge => challenge.ChallengeDescription))
                .ForMember(challengeDto => challengeDto.CityId, configExpression => configExpression.MapFrom(challenge => challenge.CityId))
                .ReverseMap();

            CreateMap<UserCountry, UserCountryDTO>()
                .ForMember(UserCountryDto => UserCountryDto.Id, configExpression => configExpression.MapFrom(UserCountry => UserCountry.Id))
                .ForMember(UserCountryDto => UserCountryDto.CountryName, configExpression => configExpression.MapFrom(UserCountry => UserCountry.CountryName))
                .ForMember(UserCountryDto => UserCountryDto.CountryStatus, configExpression => configExpression.MapFrom(UserCountry => UserCountry.CountryStatus))
                .ForMember(UserCountryDto => UserCountryDto.UserId, configExpression => configExpression.MapFrom(UserCountry => UserCountry.UserId))
                .ReverseMap();

            CreateMap<City, CityDTO>()
                .ForMember(CityDto => CityDto.Id, configExpression => configExpression.MapFrom(City => City.Id))
                .ForMember(CityDto => CityDto.CityName, configExpression => configExpression.MapFrom(City => City.CityName))
                .ForMember(CityDto => CityDto.CountryId, configExpression => configExpression.MapFrom(City => City.CountryId))
                .ReverseMap();
        }
    }
}