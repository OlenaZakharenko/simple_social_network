import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { City } from './city';

@Injectable({
  providedIn: 'root'
})
export class CityService {
constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getCitiesByCountryIdObservable (countryId: number){
    return this.http.get<City[]>(this.baseUrl+'api/cities/'+ countryId);
  }

  getCityByIdObservable (cityId: number){
    return this.http.get<City>(this.baseUrl+'api/cities/'+ cityId);
  }
}