import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CityService } from 'src/app/city/city.service';
import { City } from 'src/app/city/city';

@Component({
  selector: 'city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {
  cities$:Observable<City[]>
  cities: City[];
  countryId;
  
  constructor(private router: Router, private cityService: CityService) { }

  ngOnInit() {
    this.countryId = parseInt(sessionStorage.getItem('id'));
    console.log(this.countryId);
    console.log("countryId");
    this.cities$ = this.cityService.getCitiesByCountryIdObservable(this.countryId);
    this.cities$.subscribe(x=> {this.cities = x;} );

  }

  public setId(value){
    let ids = [this.countryId, value];
    sessionStorage.setItem('ids', JSON.stringify(ids));
  }

}
