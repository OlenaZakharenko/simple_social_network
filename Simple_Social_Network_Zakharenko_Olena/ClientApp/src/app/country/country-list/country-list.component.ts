import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CountryService } from 'src/app/country/country.service';
import { Country } from 'src/app/country/country';

@Component({
  selector: 'country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
  countries$:Observable<Country[]>
  countries: Country[];
  
  constructor(private router: Router, private countryService: CountryService) { }

  ngOnInit() {
    this.countries$ = this.countryService.getCountriesObservable();
    this.countries$.subscribe(x=> {this.countries = x;} );
  }

  public setId(value){
    sessionStorage.setItem('id', value);
  }

}
