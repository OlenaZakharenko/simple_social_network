import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Country } from './country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getCountriesObservable (){
    return this.http.get<Country[]>(this.baseUrl+'api/countries/');
  }

  getCountryByIdObservable (id: number){
    return this.http.get<Country>(this.baseUrl+'api/countries/'+id);
  }

  // getCountriesByStatusAndUserIdObservable (id: number, countryStatus: string){
  //   return this.http.get<Country>(this.baseUrl+'api/countries/'+id + countryStatus);
  // }
  
  addCurrentCountryObservable(value: object){
    return this.http.post<Country>(this.baseUrl+'api/countries', value);
 } 
}