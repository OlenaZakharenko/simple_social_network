import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { CountryService } from 'src/app/country/country.service';
import { User } from 'src/app/user/User';
import { Country } from 'src/app/country/country';

@Component({
  selector: 'country-menu',
  templateUrl: './country-menu.component.html',
  styleUrls: ['./country-menu.component.css']
})
export class CountryMenuComponent{
  countryId: number;
   constructor(private router: Router, private countryService: CountryService, private toastr: ToastrService,) { }

  addCurentCountry(){ 
    let countryId = parseInt(sessionStorage.getItem("id"));
    let userId = parseInt(localStorage.getItem("id")); 
    let value= [userId, countryId]

    this.countryId = countryId;
    
    this.countryService.addCurrentCountryObservable(value).subscribe(
      (res: any) => {
        this.toastr.success('Country has been added!');
        this.router.navigateByUrl('city-list');
    },
    err => {
      console.log(err);
    }
    );
    this.countryId = countryId;
  }

}
