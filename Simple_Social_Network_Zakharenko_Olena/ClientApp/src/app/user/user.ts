export interface User{
    id:number
    email  :string
    userName:string
    job: string
    interests: string
    skills: string
    city  :string
    age  :number
    role: string
}