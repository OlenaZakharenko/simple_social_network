import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User} from './User';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private fb: FormBuilder) { }
  
  formModel = this.fb.group({
    Email: ['', Validators.email],
    UserName: ['', Validators.required],
    Job: [''],
    Interests: [''],
    Skills: [''],
    City:[''],
    Age: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required] 
    }, { validator: this.comparePasswords })
  });

  getCurrentUserByIdObservable (){
    return this.http.get<User>(this.baseUrl+'api/users/current');
  }
    getUserByIdObservable (id: number){
      return this.http.get<User>(this.baseUrl+'api/users/'+id);
    }

    getUsersObservable (){
      return this.http.get<User[]>(this.baseUrl+'api/users');
    }

    updateUser(user: User) {
      //user.id = 1;
      return this.http.patch<User>(this.baseUrl+'api/users', user);
      
    }
    deleteUser(id){
      return this.http.delete<User>(this.baseUrl+'api/users/'+id);
    }

    register(body1){
      var body = {
        Email: this.formModel.value.Email,
        UserName: this.formModel.value.UserName,
        Job: this.formModel.value.Job,
        Interests: this.formModel.value.Interests,
        Skills: this.formModel.value.Skills,
        City: this.formModel.value.City,
        Age: this.formModel.value.Age,
        Password: this.formModel.value.Passwords.Password,
        Role:"user" 
      };
      console.log(this.baseUrl);
      return this.http.post(this.baseUrl + 'api/users/register', body);
      
    }

    login(model: User){
      return this.http.post(this.baseUrl + 'api/users/login', model);
    }

    roleMatch(allowedRoles): boolean {
      var isMatch = false;
      var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
      var userRole = payLoad.role;
      allowedRoles.forEach(element => {
        if (userRole == element) {
          isMatch = true;
          return false;
        }
      });
      return isMatch;
    }

  comparePasswords(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }
}
  