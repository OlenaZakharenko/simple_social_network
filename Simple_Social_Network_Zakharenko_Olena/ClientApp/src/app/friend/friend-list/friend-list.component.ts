import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FriendsService } from 'src/app/friend/friend.service';
import { User } from 'src/app/user/User';

@Component({
  selector: 'friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {
  friends$:Observable<User[]>
  friends: User[];
  user$:Observable<User>
  user: User;
  constructor(private router: Router, private friendService: FriendsService) { }

  ngOnInit() {
    let id = parseInt(localStorage.getItem("id"));
    console.log(id);
    this.friends$ = this.friendService.getFriendsByUserIdObservable(id);
    this.friends$.subscribe(x=> {this.friends = x;} );

  }

  public setId(value){
    sessionStorage.setItem('id', value);
  }

}
