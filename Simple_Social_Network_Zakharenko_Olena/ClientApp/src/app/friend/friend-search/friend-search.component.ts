import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FriendsService } from 'src/app/friend/friend.service';
import { User } from 'src/app/user/User';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'friend-search',
  templateUrl: './friend-search.component.html',
  styleUrls: ['./friend-search.component.css']
})
export class FriendSearchComponent implements OnInit {
  users$:Observable<User[]>
  users: User[];
  searchResults$:Observable<User[]>
  searchResults: User[];
  constructor(private router: Router,private userService: UserService, private friendsService: FriendsService, private fb: FormBuilder) { }

  searchFormModel = this.fb.group({
    UserName: [''],
    MinAge: [''],
    MaxAge: [''],
    Job:[''],
    City:['']
  });

  ngOnInit() {
    let id = parseInt(localStorage.getItem("id"));
    this.users$ = this.userService.getUsersObservable();
    this.users$.subscribe(x=> {this.users = x;} );
    this.searchFormModel.reset();
  }
  search(searchModel:any) {
    this.searchResults$ =  this.friendsService.searchFriendsObservable(searchModel);
    this.searchResults$.subscribe(
      (res: any) => {
          this.searchResults = res;
          console.log(this.searchResults);
          this.searchFormModel.reset();
      },
      err => {
        console.log(err);
      }
    );
  }
  
  addFriend(model){
    let id = parseInt(localStorage.getItem("id"));
    let value = [id, model.id]
    this.friendsService.addFriendObservable(value).subscribe();
  }
}
