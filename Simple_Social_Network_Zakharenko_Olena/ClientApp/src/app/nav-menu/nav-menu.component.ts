import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  isExpanded = false;
  isAdmin = false;
  IsAuthorized = false;

  constructor(private router: Router) {}

  ngOnInit(){
    this.isAdmin = this.checkIsAdmin();
    this.IsAuthorized = this.checkIsAuthorized();
  }

  collapse() {
    this.isExpanded = false;
  }

checkIsAdmin():boolean{
  var token = localStorage.getItem('token');
  if (!token) return false;
  
  var payLoad = JSON.parse(atob(token.split('.')[1]));
      var userRole = payLoad.role;
        if (userRole == 'admin') {
          return true;
        }
      return false;
}
  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  checkIsAuthorized():boolean{
    var token = localStorage.getItem('token');
    console.log(token);
          if (token == null || token == undefined) {
            return false;
          }
        return true;
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
