import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router'; 

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ProfileComponent } from './user-profile/user-profile.component';
import { UserService } from './user/user.service';
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoginComponent } from './auth/login/login.component';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { ChangeProfileComponent } from './user-profile/change-profile/change-profile.component';
import { FriendProfileComponent } from './friend/friend-profile/friend-profile.component';
import { FriendListComponent } from './friend/friend-list/friend-list.component';
import { FriendSearchComponent } from './friend/friend-search/friend-search.component';
import { ForbiddenComponent } from './auth/forbidden/forbidden.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './Auth/auth.guard';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { CountryListComponent } from './country/country-list/country-list.component';
import { CountryMenuComponent } from './country/country-menu/country-menu.component';
import { CityListComponent } from './city/city-list/city-list.component';
import { ChallengeComponent } from './challenge/challenge.component';
import { ChallengeListComponent } from './challenge/challenge-list/challenge-list.component';
import { ChallengeUploadComponent } from './challenge/challenge-upload.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    ProfileComponent,
    RegistrationComponent,
    LoginComponent,
    AdminPanelComponent,
    ChangeProfileComponent,
    FriendProfileComponent,
    FriendListComponent,
    FriendSearchComponent,
    ForbiddenComponent,
    CountryListComponent,
    CountryMenuComponent,
    CityListComponent,
    ChallengeListComponent,
    ChallengeComponent,
    ChallengeUploadComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar: true
    }),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full'},
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'registration', component: RegistrationComponent},
      { path: 'user-profile', component:  ProfileComponent,canActivate:[AuthGuard]},
      { path: 'login', component: LoginComponent},
      { path: 'nav-menu', component: NavMenuComponent},
      { path: 'change-profile', component: ChangeProfileComponent},
      { path: 'friend-profile', component: FriendProfileComponent, canActivate:[AuthGuard]},
      { path: 'forbidden',component:ForbiddenComponent},
      { path: 'friend-list', component: FriendListComponent, canActivate:[AuthGuard]},
      { path: 'friend-search', component: FriendSearchComponent, canActivate:[AuthGuard]},
      { path: 'country-list', component: CountryListComponent, canActivate:[AuthGuard]},
      { path: 'country-menu', component: CountryMenuComponent, canActivate:[AuthGuard]},
      { path: 'city-list', component: CityListComponent, canActivate:[AuthGuard]},
      { path: 'challenge-list', component: ChallengeListComponent, canActivate:[AuthGuard]},
      { path: 'app-challenge-upload', component: ChallengeUploadComponent, canActivate:[AuthGuard]},
      { path: 'app-challenge', component: ChallengeComponent, canActivate:[AuthGuard]},
      { path: 'admin',component:AdminPanelComponent, canActivate:[AuthGuard],data :{permittedRoles:['admin']}}
    ])
  ],
  providers: [UserService,
     {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
   },
  ToastrService
],

  bootstrap: [AppComponent]
})
export class AppModule { }