import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { User } from 'src/app/user/User';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.css']
})
export class ChangeProfileComponent implements OnInit {

  constructor(public service: UserService, private toastr: ToastrService, private router:Router, private fb: FormBuilder) { }

  user$:Observable<User>
  user:User
  profileFormModel = this.fb.group({
    userName: [''],
    age: [''],
    email:[''],
    password: [''],
    skills: [''],
    city:[''],
    interests:[''],
    job:[''],
});
  ngOnInit() {
    let id = parseInt(localStorage.getItem('id'));
    this.user$ = this.service.getUserByIdObservable(id);
    this.user$.subscribe(x=> {this.user = x;} );
  }

  onSubmit(userModel:User) {
    userModel.userName = this.user.userName;
    this.service.updateUser(userModel).subscribe(
      (res: any) => {
          this.profileFormModel.reset();
          this.toastr.success('Information has been updated!');
          this.router.navigateByUrl('/user-profile');
      },
      err => {
        console.log(err);
      }
    );
  }

}