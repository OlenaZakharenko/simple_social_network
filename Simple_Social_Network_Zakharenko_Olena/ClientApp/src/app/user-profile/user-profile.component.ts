import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../user/user';
import { UserService } from '../user/user.service';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class ProfileComponent implements OnInit {
  user$:Observable<User>
  user: User;
  

  constructor(private router: Router, private service: UserService) { }

  ngOnInit() {
    let id = parseInt(localStorage.getItem("id"));
    this.user$ = this.service.getUserByIdObservable(id);
    this.user$.subscribe(x=> {this.user = x;} );
  }
   
}
