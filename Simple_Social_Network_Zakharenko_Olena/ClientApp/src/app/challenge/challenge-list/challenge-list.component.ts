import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChallengeService } from 'src/app/challenge/challenge.service';
import { Challenge } from 'src/app/challenge/challenge';

@Component({
  selector: 'challenge-list',
  templateUrl: './challenge-list.component.html',
  styleUrls: ['./challenge-list.component.css']
})
export class ChallengeListComponent implements OnInit {
  // @Input() countryId: number;
  challenges$:Observable<Challenge[]>
  challenges: Challenge[];
  isCurrentCountry = false;
  
  constructor(private router: Router, private challengeService: ChallengeService) { }

  ngOnInit() {
    let userId = parseInt(localStorage.getItem("id")); 

    let storedIds = sessionStorage.getItem('ids');
    let ids = JSON.parse(storedIds);
    let countryId = ids[0];
    console.log(countryId)
    this.challengeService.isCountryCurrentObservable(userId, countryId).subscribe(
      (isCurrent: boolean) => {
        this.isCurrentCountry = isCurrent;
        console.log(this.isCurrentCountry);
      },
      (error: any) => {
        console.log(error);
      }
    );

    let cityId = ids[1];;
    this.challenges$ = this.challengeService.getChallengesByCityIdObservable(cityId);
    this.challenges$.subscribe(x=> {this.challenges = x;} );
  }
}