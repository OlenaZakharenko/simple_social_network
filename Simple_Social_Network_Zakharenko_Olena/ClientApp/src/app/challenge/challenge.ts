export interface Challenge{
    challengeDescription  :string
    challengeStatus:string
    id: number
}