import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Challenge } from './challenge';
import { FileUploaded } from './fileUploaded';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {
constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getChallengesByCityIdObservable (cityId: number){
    return this.http.get<Challenge[]>(this.baseUrl+'api/challenges/'+ cityId);
  }

  isCountryCurrentObservable(userId: number, countryId: number){
    const params = new HttpParams()
    .set('userId', userId.toString())
    .set('countryId', countryId.toString());

  return this.http.get<boolean>(`${this.baseUrl}api/challenges/isCurrent`, { params });
  }

  submitReportForReview(file: File, userId: number, taskId: number) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('userId', userId.toString());
    formData.append('taskId', taskId.toString());
  
    return this.http.post<FileUploaded>(this.baseUrl + 'api/challenges/submitFile', formData);
  }
}