import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { ChallengeService } from 'src/app/challenge/challenge.service';
import { Challenge } from 'src/app/challenge/challenge';


@Component({
    selector: 'app-challenge-upload',
    template: `
    <div class="app-challenge-container challenge-upload-component" [class.enlarge]="showFullDescription" (mousedown)="toggleDescription($event)">
    <div class="app-challenge-content">
      <ng-container *ngIf="!uploadedFile || showFullDescription">
        {{ showFullDescription ? description : shortDescription }}
      </ng-container>
      <div class="file-upload-container" *ngIf="showFullDescription">
        <div class="file-upload-content">
          <input type="file" (change)="onFileSelected($event)" accept=".pdf,.doc,.docx" *ngIf="!uploadedFile">
          <div class="file-info" *ngIf="uploadedFile">
            <img src="file-icon.png" class="file-icon">
            <div class="file-details">
              <span class="file-name">{{ uploadedFile.name }}</span>
              <span class="file-extension">{{ uploadedFile.extension }}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <button class="submit-button" *ngIf="uploadedFile" (click)="submitForReview()">Submit for review</button>
  </div>
  
   
  
  `,

  styleUrls: ['./challenge-list/challenge-list.component.css']
  })
  export class ChallengeUploadComponent implements OnInit {
    
    @Input() description: string;
    @Input() taskId: number;

    showFullDescription = false;
    shortDescription: string;
    uploadedFile: { name: string, extension: string } = null;
    file: File; 

    constructor(private router: Router, private challengeService: ChallengeService) { }

  ngOnInit() {
    if (this.description.length > 30) {
      this.shortDescription = this.description.slice(0, 30) + '...';
    } else {
      this.shortDescription = this.description;
    }
  }

  toggleDescription(event: Event) {
    if ((<HTMLElement>event.target).tagName !== 'INPUT') {
      this.showFullDescription = !this.showFullDescription;
    }
  }

  onFileSelected(event: any) {
    this.file = event.target.files[0];
    if (this.file) {
      const filename = this.file.name;
      const extensionIndex = filename.lastIndexOf('.');
      const nameWithoutExtension = extensionIndex !== -1 ? filename.slice(0, extensionIndex) : filename;
      this.uploadedFile = {
        name: nameWithoutExtension,
        extension: this.getFileExtension(filename)
      };
    } else {
      this.uploadedFile = null;
    }
    setTimeout(() => {
      this.showFullDescription = this.uploadedFile !== null;
    });
  }


  submitForReview() {
    if (this.uploadedFile) {
      var userId = parseInt(localStorage.getItem('id'), 10);

      this.challengeService.submitReportForReview(this.file, userId, this.taskId).subscribe(
        (response) => {
          console.log('File submitted for review:', response.fileName);
        },
        (error) => {
          console.error('Error submitting file for review:', error);
        }
      );
    } else {
      console.error('No file selected or taskId is missing');
    }
  }

  getFileExtension(filename: string): string {
    const extension = filename.split('.').pop();
    return extension ? extension.toLowerCase() : '';
  }
}