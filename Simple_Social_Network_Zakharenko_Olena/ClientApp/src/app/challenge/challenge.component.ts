import { Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'app-challenge',
    template: `
      <div class="app-challenge-container challenge-component"
        [class.enlarge]="showFullDescription"
        (click)="toggleDescription()"
      >
      <div class="app-challenge-content">
        {{ showFullDescription ? description : shortDescription }}
        </div>
      </div>
    `,

    styleUrls: ['./challenge-list/challenge-list.component.css'],
  })
  export class ChallengeComponent implements OnInit {
    @Input() description: string;
  
    shortDescription: string;
    showFullDescription = false;
  
    ngOnInit() {
      if (this.description.length > 30) {
        this.shortDescription = this.description.slice(0, 30) + '...';
      } else {
        this.shortDescription = this.description;
      }
    }
  
    toggleDescription() {
      this.showFullDescription = !this.showFullDescription;
      }
    }
  